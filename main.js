class MyTimer extends HTMLElement {

    constructor() {
        super();

        // this.attachShadow({ mode: 'open' });

        const divElement = document.createElement('div');

        divElement.setAttribute('class', 'box');
        divElement.textContent = `COMPONENTE 01`

        this.asyncProcess();
        // this.syncProcess();

        this.append(divElement, this.getStyle());

        // this.shadowRoot.append(divElement);
    }

    asyncProcess() {
        setInterval(() => {
            console.warn('my timer say 01!');
        }, 500);
    }

    syncProcess() {
        for (let index = 0; index <= 1e5; index++) {
            console.count('executing process!');

        }
    }

    getStyle() {
        const styleElement = document.createElement('style');
        styleElement.textContent = `
            .box{
                background-color: pink;
                padding: 1rem;
                font-family: Arial;
            }
        `;
        return styleElement;
    }


}

class MyTimer2 extends HTMLElement {

    constructor() {
        super();

        // this.attachShadow({ mode: 'open' });

        const divElement = document.createElement('div');

        divElement.setAttribute('class', 'box');
        divElement.textContent = `COMPONENTE 02`

        this.asyncProcess();
        // this.syncProcess();

        this.append(divElement, this.getStyle());

        // this.shadowRoot.append(divElement);
    }

    asyncProcess() {
        setInterval(() => {
            console.error('my timer say 02!');
        }, 500);
    }

    syncProcess() {
        for (let index = 0; index <= 1e5; index++) {
            console.count('executing process!');

        }
    }

    getStyle() {
        const styleElement = document.createElement('style');
        styleElement.textContent = `
            .box{
                background-color: pink;
                padding: 1rem;
                font-family: Arial;
            }
        `;
        return styleElement;
    }


}

class MyTimer3 extends HTMLElement {

    constructor() {
        super();

        // this.attachShadow({ mode: 'open' });

        const divElement = document.createElement('div');

        divElement.setAttribute('class', 'box');
        divElement.textContent = `COMPONENTE 03`

        this.asyncProcess();
        // this.syncProcess();

        this.append(divElement, this.getStyle());

        // this.shadowRoot.append(divElement);
    }

    asyncProcess() {
        setInterval(() => {
            console.info('my timer say 03!');
        }, 500);
    }

    syncProcess() {
        for (let index = 0; index <= 1e5; index++) {
            console.count('executing process!');

        }
    }

    getStyle() {
        const styleElement = document.createElement('style');
        styleElement.textContent = `
            .box{
                background-color: pink;
                padding: 1rem;
                font-family: Arial;
            }
        `;
        return styleElement;
    }


}

customElements.define('bcd-timer', MyTimer);
customElements.define('bcd-timer-2', MyTimer2);

setTimeout(() => {

    console.log('REMOVED! 01');
    const bcdTimer = document.querySelector('bcd-timer');
    bcdTimer.remove();

}, 2000);

setTimeout(() => {

    console.log('REMOVED! 02');
    const bcdTimer2 = document.querySelector('bcd-timer-2');

    bcdTimer2.remove();


}, 4000);

setTimeout(() => {

    const bcdTimer3 = document.createElement('bcd-timer-3');
    customElements.define('bcd-timer-3', MyTimer3);

    document.body.append(bcdTimer3);


}, 6000);


setTimeout(() => {
    console.log('REMOVED! 03');
    const bcdTimer3 = document.querySelector('bcd-timer-3');
    bcdTimer3.remove();


}, 8000);
